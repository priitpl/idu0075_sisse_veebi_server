
package ee.pold.idu0075.shop;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.pold.idu0075.shop package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCategoriesListRequest_QNAME = new QName("http://idu0075.pold.ee/shop", "GetCategoriesListRequest");
    private final static QName _CalculatePriceWithDiscountResponse_QNAME = new QName("http://idu0075.pold.ee/shop", "CalculatePriceWithDiscountResponse");
    private final static QName _GetCategoriesListResponse_QNAME = new QName("http://idu0075.pold.ee/shop", "GetCategoriesListResponse");
    private final static QName _GetShopsIdentitiesResponse_QNAME = new QName("http://idu0075.pold.ee/shop", "GetShopsIdentitiesResponse");
    private final static QName _CalculatePriceWithDiscountRequest_QNAME = new QName("http://idu0075.pold.ee/shop", "CalculatePriceWithDiscountRequest");
    private final static QName _GetProductStockCountRequest_QNAME = new QName("http://idu0075.pold.ee/shop", "GetProductStockCountRequest");
    private final static QName _GetShopsIdentitiesRequest_QNAME = new QName("http://idu0075.pold.ee/shop", "GetShopsIdentitiesRequest");
    private final static QName _GetProductStockCountResponse_QNAME = new QName("http://idu0075.pold.ee/shop", "GetProductStockCountResponse");
    private final static QName _FindProductsResponse_QNAME = new QName("http://idu0075.pold.ee/shop", "FindProductsResponse");
    private final static QName _FindProductsRequest_QNAME = new QName("http://idu0075.pold.ee/shop", "FindProductsRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.pold.idu0075.shop
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetShopsIdentitiesResponseType }
     * 
     */
    public GetShopsIdentitiesResponseType createGetShopsIdentitiesResponseType() {
        return new GetShopsIdentitiesResponseType();
    }

    /**
     * Create an instance of {@link FindProductsResponseType }
     * 
     */
    public FindProductsResponseType createFindProductsResponseType() {
        return new FindProductsResponseType();
    }

    /**
     * Create an instance of {@link GetCategoriesListRequestType }
     * 
     */
    public GetCategoriesListRequestType createGetCategoriesListRequestType() {
        return new GetCategoriesListRequestType();
    }

    /**
     * Create an instance of {@link GetProductStockCountRequestType }
     * 
     */
    public GetProductStockCountRequestType createGetProductStockCountRequestType() {
        return new GetProductStockCountRequestType();
    }

    /**
     * Create an instance of {@link FindProductsRequestType }
     * 
     */
    public FindProductsRequestType createFindProductsRequestType() {
        return new FindProductsRequestType();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link Category }
     * 
     */
    public Category createCategory() {
        return new Category();
    }

    /**
     * Create an instance of {@link Categories }
     * 
     */
    public Categories createCategories() {
        return new Categories();
    }

    /**
     * Create an instance of {@link ShopIdentity }
     * 
     */
    public ShopIdentity createShopIdentity() {
        return new ShopIdentity();
    }

    /**
     * Create an instance of {@link GetCategoriesListResponseType }
     * 
     */
    public GetCategoriesListResponseType createGetCategoriesListResponseType() {
        return new GetCategoriesListResponseType();
    }

    /**
     * Create an instance of {@link CalculatePriceWithDiscountResponseType }
     * 
     */
    public CalculatePriceWithDiscountResponseType createCalculatePriceWithDiscountResponseType() {
        return new CalculatePriceWithDiscountResponseType();
    }

    /**
     * Create an instance of {@link CalculatePriceWithDiscountRequestType }
     * 
     */
    public CalculatePriceWithDiscountRequestType createCalculatePriceWithDiscountRequestType() {
        return new CalculatePriceWithDiscountRequestType();
    }

    /**
     * Create an instance of {@link GetShopsIdentitiesRequestType }
     * 
     */
    public GetShopsIdentitiesRequestType createGetShopsIdentitiesRequestType() {
        return new GetShopsIdentitiesRequestType();
    }

    /**
     * Create an instance of {@link GetProductStockCountResponseType }
     * 
     */
    public GetProductStockCountResponseType createGetProductStockCountResponseType() {
        return new GetProductStockCountResponseType();
    }

    /**
     * Create an instance of {@link GetShopsIdentitiesResponseType.Shops }
     * 
     */
    public GetShopsIdentitiesResponseType.Shops createGetShopsIdentitiesResponseTypeShops() {
        return new GetShopsIdentitiesResponseType.Shops();
    }

    /**
     * Create an instance of {@link FindProductsResponseType.Products }
     * 
     */
    public FindProductsResponseType.Products createFindProductsResponseTypeProducts() {
        return new FindProductsResponseType.Products();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCategoriesListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "GetCategoriesListRequest")
    public JAXBElement<GetCategoriesListRequestType> createGetCategoriesListRequest(GetCategoriesListRequestType value) {
        return new JAXBElement<GetCategoriesListRequestType>(_GetCategoriesListRequest_QNAME, GetCategoriesListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculatePriceWithDiscountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "CalculatePriceWithDiscountResponse")
    public JAXBElement<CalculatePriceWithDiscountResponseType> createCalculatePriceWithDiscountResponse(CalculatePriceWithDiscountResponseType value) {
        return new JAXBElement<CalculatePriceWithDiscountResponseType>(_CalculatePriceWithDiscountResponse_QNAME, CalculatePriceWithDiscountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCategoriesListResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "GetCategoriesListResponse")
    public JAXBElement<GetCategoriesListResponseType> createGetCategoriesListResponse(GetCategoriesListResponseType value) {
        return new JAXBElement<GetCategoriesListResponseType>(_GetCategoriesListResponse_QNAME, GetCategoriesListResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetShopsIdentitiesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "GetShopsIdentitiesResponse")
    public JAXBElement<GetShopsIdentitiesResponseType> createGetShopsIdentitiesResponse(GetShopsIdentitiesResponseType value) {
        return new JAXBElement<GetShopsIdentitiesResponseType>(_GetShopsIdentitiesResponse_QNAME, GetShopsIdentitiesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculatePriceWithDiscountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "CalculatePriceWithDiscountRequest")
    public JAXBElement<CalculatePriceWithDiscountRequestType> createCalculatePriceWithDiscountRequest(CalculatePriceWithDiscountRequestType value) {
        return new JAXBElement<CalculatePriceWithDiscountRequestType>(_CalculatePriceWithDiscountRequest_QNAME, CalculatePriceWithDiscountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductStockCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "GetProductStockCountRequest")
    public JAXBElement<GetProductStockCountRequestType> createGetProductStockCountRequest(GetProductStockCountRequestType value) {
        return new JAXBElement<GetProductStockCountRequestType>(_GetProductStockCountRequest_QNAME, GetProductStockCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetShopsIdentitiesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "GetShopsIdentitiesRequest")
    public JAXBElement<GetShopsIdentitiesRequestType> createGetShopsIdentitiesRequest(GetShopsIdentitiesRequestType value) {
        return new JAXBElement<GetShopsIdentitiesRequestType>(_GetShopsIdentitiesRequest_QNAME, GetShopsIdentitiesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductStockCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "GetProductStockCountResponse")
    public JAXBElement<GetProductStockCountResponseType> createGetProductStockCountResponse(GetProductStockCountResponseType value) {
        return new JAXBElement<GetProductStockCountResponseType>(_GetProductStockCountResponse_QNAME, GetProductStockCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProductsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "FindProductsResponse")
    public JAXBElement<FindProductsResponseType> createFindProductsResponse(FindProductsResponseType value) {
        return new JAXBElement<FindProductsResponseType>(_FindProductsResponse_QNAME, FindProductsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProductsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/shop", name = "FindProductsRequest")
    public JAXBElement<FindProductsRequestType> createFindProductsRequest(FindProductsRequestType value) {
        return new JAXBElement<FindProductsRequestType>(_FindProductsRequest_QNAME, FindProductsRequestType.class, null, value);
    }

}
