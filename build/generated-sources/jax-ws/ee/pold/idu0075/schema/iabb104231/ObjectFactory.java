
package ee.pold.idu0075.schema.iabb104231;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.pold.idu0075.schema.iabb104231 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetKuulutusRequest_QNAME = new QName("http://idu0075.pold.ee/schema/iabb104231", "getKuulutusRequest");
    private final static QName _AddKorterRequest_QNAME = new QName("http://idu0075.pold.ee/schema/iabb104231", "addKorterRequest");
    private final static QName _AddKorterResponse_QNAME = new QName("http://idu0075.pold.ee/schema/iabb104231", "addKorterResponse");
    private final static QName _GetKuulutusResponse_QNAME = new QName("http://idu0075.pold.ee/schema/iabb104231", "getKuulutusResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.pold.idu0075.schema.iabb104231
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetKuulutusResponseType }
     * 
     */
    public GetKuulutusResponseType createGetKuulutusResponseType() {
        return new GetKuulutusResponseType();
    }

    /**
     * Create an instance of {@link AddKorterRequestType }
     * 
     */
    public AddKorterRequestType createAddKorterRequestType() {
        return new AddKorterRequestType();
    }

    /**
     * Create an instance of {@link GetKuulutusRequestType }
     * 
     */
    public GetKuulutusRequestType createGetKuulutusRequestType() {
        return new GetKuulutusRequestType();
    }

    /**
     * Create an instance of {@link AddKorterResponseType }
     * 
     */
    public AddKorterResponseType createAddKorterResponseType() {
        return new AddKorterResponseType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link KorterType }
     * 
     */
    public KorterType createKorterType() {
        return new KorterType();
    }

    /**
     * Create an instance of {@link GetKuulutusResponseType.Korterid }
     * 
     */
    public GetKuulutusResponseType.Korterid createGetKuulutusResponseTypeKorterid() {
        return new GetKuulutusResponseType.Korterid();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetKuulutusRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/schema/iabb104231", name = "getKuulutusRequest")
    public JAXBElement<GetKuulutusRequestType> createGetKuulutusRequest(GetKuulutusRequestType value) {
        return new JAXBElement<GetKuulutusRequestType>(_GetKuulutusRequest_QNAME, GetKuulutusRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddKorterRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/schema/iabb104231", name = "addKorterRequest")
    public JAXBElement<AddKorterRequestType> createAddKorterRequest(AddKorterRequestType value) {
        return new JAXBElement<AddKorterRequestType>(_AddKorterRequest_QNAME, AddKorterRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddKorterResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/schema/iabb104231", name = "addKorterResponse")
    public JAXBElement<AddKorterResponseType> createAddKorterResponse(AddKorterResponseType value) {
        return new JAXBElement<AddKorterResponseType>(_AddKorterResponse_QNAME, AddKorterResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetKuulutusResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://idu0075.pold.ee/schema/iabb104231", name = "getKuulutusResponse")
    public JAXBElement<GetKuulutusResponseType> createGetKuulutusResponse(GetKuulutusResponseType value) {
        return new JAXBElement<GetKuulutusResponseType>(_GetKuulutusResponse_QNAME, GetKuulutusResponseType.class, null, value);
    }

}
