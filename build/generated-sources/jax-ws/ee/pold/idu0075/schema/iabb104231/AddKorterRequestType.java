
package ee.pold.idu0075.schema.iabb104231;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addKorterRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addKorterRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Korter" type="{http://idu0075.pold.ee/schema/iabb104231}KorterType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addKorterRequestType", propOrder = {
    "korter"
})
public class AddKorterRequestType {

    @XmlElement(name = "Korter", required = true)
    protected KorterType korter;

    /**
     * Gets the value of the korter property.
     * 
     * @return
     *     possible object is
     *     {@link KorterType }
     *     
     */
    public KorterType getKorter() {
        return korter;
    }

    /**
     * Sets the value of the korter property.
     * 
     * @param value
     *     allowed object is
     *     {@link KorterType }
     *     
     */
    public void setKorter(KorterType value) {
        this.korter = value;
    }

}
