
package ee.pold.idu0075.schema.iabb104231;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getKuulutusResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getKuulutusResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Korterid">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Korter" type="{http://idu0075.pold.ee/schema/iabb104231}KorterType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getKuulutusResponseType", propOrder = {
    "korterid"
})
public class GetKuulutusResponseType {

    @XmlElement(name = "Korterid", required = true)
    protected GetKuulutusResponseType.Korterid korterid;

    /**
     * Gets the value of the korterid property.
     * 
     * @return
     *     possible object is
     *     {@link GetKuulutusResponseType.Korterid }
     *     
     */
    public GetKuulutusResponseType.Korterid getKorterid() {
        return korterid;
    }

    /**
     * Sets the value of the korterid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetKuulutusResponseType.Korterid }
     *     
     */
    public void setKorterid(GetKuulutusResponseType.Korterid value) {
        this.korterid = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Korter" type="{http://idu0075.pold.ee/schema/iabb104231}KorterType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "korter"
    })
    public static class Korterid {

        @XmlElement(name = "Korter", required = true)
        protected KorterType korter;

        /**
         * Gets the value of the korter property.
         * 
         * @return
         *     possible object is
         *     {@link KorterType }
         *     
         */
        public KorterType getKorter() {
            return korter;
        }

        /**
         * Sets the value of the korter property.
         * 
         * @param value
         *     allowed object is
         *     {@link KorterType }
         *     
         */
        public void setKorter(KorterType value) {
            this.korter = value;
        }

    }

}
