
package ee.pold.idu0075.schema.iabb104231;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Linn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Tanav" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="KorteriNr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressType", propOrder = {
    "linn",
    "tanav",
    "korteriNr"
})
public class AddressType {

    @XmlElement(name = "Linn")
    protected String linn;
    @XmlElement(name = "Tanav")
    protected String tanav;
    @XmlElement(name = "KorteriNr")
    protected String korteriNr;

    /**
     * Gets the value of the linn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinn() {
        return linn;
    }

    /**
     * Sets the value of the linn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinn(String value) {
        this.linn = value;
    }

    /**
     * Gets the value of the tanav property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTanav() {
        return tanav;
    }

    /**
     * Sets the value of the tanav property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTanav(String value) {
        this.tanav = value;
    }

    /**
     * Gets the value of the korteriNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorteriNr() {
        return korteriNr;
    }

    /**
     * Sets the value of the korteriNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorteriNr(String value) {
        this.korteriNr = value;
    }

}
