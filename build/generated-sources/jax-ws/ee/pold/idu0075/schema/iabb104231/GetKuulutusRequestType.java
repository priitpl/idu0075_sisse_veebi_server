
package ee.pold.idu0075.schema.iabb104231;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getKuulutusRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getKuulutusRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="Linn" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="TuadeArv" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
 *         &lt;element name="MaxHind" type="{http://idu0075.pold.ee/schema/iabb104231}priceType" minOccurs="0"/>
 *         &lt;element name="Maakler" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getKuulutusRequestType", propOrder = {

})
public class GetKuulutusRequestType {

    @XmlElement(name = "Linn", required = true)
    protected Object linn;
    @XmlElement(name = "TuadeArv")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger tuadeArv;
    @XmlElement(name = "MaxHind")
    protected BigDecimal maxHind;
    @XmlElement(name = "Maakler")
    protected Object maakler;

    /**
     * Gets the value of the linn property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getLinn() {
        return linn;
    }

    /**
     * Sets the value of the linn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setLinn(Object value) {
        this.linn = value;
    }

    /**
     * Gets the value of the tuadeArv property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTuadeArv() {
        return tuadeArv;
    }

    /**
     * Sets the value of the tuadeArv property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTuadeArv(BigInteger value) {
        this.tuadeArv = value;
    }

    /**
     * Gets the value of the maxHind property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxHind() {
        return maxHind;
    }

    /**
     * Sets the value of the maxHind property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxHind(BigDecimal value) {
        this.maxHind = value;
    }

    /**
     * Gets the value of the maakler property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getMaakler() {
        return maakler;
    }

    /**
     * Sets the value of the maakler property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setMaakler(Object value) {
        this.maakler = value;
    }

}
