
package ee.pold.idu0075.schema.iabb104231;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KorterType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KorterType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Address" type="{http://idu0075.pold.ee/schema/iabb104231}AddressType" minOccurs="0"/>
 *         &lt;element name="KorteriSuurus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Hind" type="{http://idu0075.pold.ee/schema/iabb104231}priceType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KorterType", propOrder = {
    "address",
    "korteriSuurus",
    "hind"
})
public class KorterType {

    @XmlElement(name = "Address")
    protected AddressType address;
    @XmlElement(name = "KorteriSuurus", required = true)
    protected String korteriSuurus;
    @XmlElement(name = "Hind", required = true)
    protected BigDecimal hind;

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setAddress(AddressType value) {
        this.address = value;
    }

    /**
     * Gets the value of the korteriSuurus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorteriSuurus() {
        return korteriSuurus;
    }

    /**
     * Sets the value of the korteriSuurus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorteriSuurus(String value) {
        this.korteriSuurus = value;
    }

    /**
     * Gets the value of the hind property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHind() {
        return hind;
    }

    /**
     * Sets the value of the hind property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHind(BigDecimal value) {
        this.hind = value;
    }

}
