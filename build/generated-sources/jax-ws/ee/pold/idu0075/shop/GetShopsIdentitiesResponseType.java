
package ee.pold.idu0075.shop;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetShopsIdentitiesResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetShopsIdentitiesResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Shops">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://idu0075.pold.ee/shop}shopIdentity" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetShopsIdentitiesResponseType", propOrder = {
    "shops"
})
public class GetShopsIdentitiesResponseType {

    @XmlElement(name = "Shops", required = true)
    protected GetShopsIdentitiesResponseType.Shops shops;

    /**
     * Gets the value of the shops property.
     * 
     * @return
     *     possible object is
     *     {@link GetShopsIdentitiesResponseType.Shops }
     *     
     */
    public GetShopsIdentitiesResponseType.Shops getShops() {
        return shops;
    }

    /**
     * Sets the value of the shops property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetShopsIdentitiesResponseType.Shops }
     *     
     */
    public void setShops(GetShopsIdentitiesResponseType.Shops value) {
        this.shops = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://idu0075.pold.ee/shop}shopIdentity" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shopIdentity"
    })
    public static class Shops {

        @XmlElement(namespace = "http://idu0075.pold.ee/shop")
        protected List<ShopIdentity> shopIdentity;

        /**
         * Gets the value of the shopIdentity property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the shopIdentity property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getShopIdentity().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ShopIdentity }
         * 
         * 
         */
        public List<ShopIdentity> getShopIdentity() {
            if (shopIdentity == null) {
                shopIdentity = new ArrayList<ShopIdentity>();
            }
            return this.shopIdentity;
        }

    }

}
