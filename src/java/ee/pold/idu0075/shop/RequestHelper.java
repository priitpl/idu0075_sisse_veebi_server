package ee.pold.idu0075.shop;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequestHelper {

	public static String sendGetRequest(String urlString){
		String responseData = "";
		try {
			URL url = new URL(urlString);
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			url = uri.toURL();
			String urlStr = url.toString();
			
			System.out.println("Send request to: " + urlStr);
			
		    URLConnection yc = url.openConnection();
		    yc.setConnectTimeout(2000);
		    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		    String inputLine;
		
		    while ((inputLine = in.readLine()) != null) {
		    	responseData += inputLine;
		    }
		    in.close();
		} catch(Exception e){
			System.out.println(e.toString());
		}
		return responseData;
	}
	
	
	public static String sendPostRequest(String urlString){
		String responseData = "";
		try {
			URL url = new URL(urlString);
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			url = uri.toURL();
			String urlStr = url.toString();
			
			System.out.println("Send request to: " + urlStr);
			
		    URLConnection yc = url.openConnection();
		    yc.setConnectTimeout(2000);
		    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		    String inputLine;
		
		    while ((inputLine = in.readLine()) != null) {
		    	responseData += inputLine;
		    }
		    in.close();
		} catch(Exception e){
			System.out.println(e.toString());
		}
		return null;
	}
	
	public static String sendPostRequest(String urlString, HashMap<String, String> keys){
		String responseData = "";
		try {
			URL url = new URL(urlString);
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			url = uri.toURL();
			String urlStr = url.toString();
			
			System.out.println("Send request to: " + urlStr);
			
		    URLConnection yc = url.openConnection();
		    yc.setConnectTimeout(2000);
		    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		    String inputLine;
		
		    while ((inputLine = in.readLine()) != null) {
		    	responseData += inputLine;
		    }
		    in.close();
		} catch(Exception e){
			System.out.println(e.toString());
		}
		return "";
	}
	
	public static String parseUnicodeJsonString(String jsonString) {
		String s = "";
		int i;
		for (i = 0; i < jsonString.length() - 5; i++) {
			String part = jsonString.substring(i, i + 6);
			Matcher m = Pattern.compile("\\\\u([0-9a-zA-Z]{4})").matcher(part);
			if (m.matches()) {
				s += Character.toString((char) Integer.parseInt(m.group(1), 16));
				i += 5;
				continue;
			}
			s += jsonString.charAt(i);
		}
		while (i < jsonString.length()) {
			s += jsonString.charAt(i++);
		}
		return s;
	}
}
