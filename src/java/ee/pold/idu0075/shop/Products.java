/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ee.pold.idu0075.shop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author priitpl
 */
public class Products {
    private List<Product> products = new ArrayList<Product>();
    
    public Products(){
    }
    
    public Product create(String name, int categoryId, double price){
        Product p = create(name, price);
        p.setName(name);
        return p;
    }
    public Product create(String name){
        Product p = new Product();
        p.setName(name);
        return p;
    }
    public Product create(String name, double price){
        Product p = new Product();
        p.setName(name);
        p.setPrice(BigDecimal.valueOf(price));
        return p;
    }
    
    public Products add(Product p){
        products.add(p);
        return this;
    }
    
    public ArrayList<Product> findProducts(int categoryId, double minPrice, double maxPrice){
        ArrayList<Product> found = new ArrayList<Product>();
        return found;
    }
    public ArrayList<Product> findProducts(int categoryId){
        ArrayList<Product> found = new ArrayList<Product>();
        return found;
    }
    
    public ArrayList<Product> findProductsMinPrice(double price){
        ArrayList<Product> found = new ArrayList<Product>();
        return found;
    }
    public ArrayList<Product> findProductsMaxPrice(double price){
        ArrayList<Product> found = new ArrayList<Product>();
        return found;
    }
    public ArrayList<Product> findProducts(double minPrice, double maxPrice){
        ArrayList<Product> found = new ArrayList<Product>();
        return found;
    }
    
}
