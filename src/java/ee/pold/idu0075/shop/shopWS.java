/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ee.pold.idu0075.shop;

import ee.pold.idu0075.shop.resources.AbstractIdentity;
import java.util.ArrayList;
import javax.jws.WebService;
import ee.pold.idu0075.shop.resources.Request;
import ee.pold.idu0075.shop.resources.amazon.AmazonIdentity;
import ee.pold.idu0075.shop.resources.ebay.EbayIdentity;
import java.math.BigInteger;
import java.util.Iterator;
/**
 *
 * @author priitpl
 */
@WebService(serviceName = "shopService", portName = "shopPort", endpointInterface = "ee.pold.idu0075.wsdl.shopapp.ShopPortType", targetNamespace = "http://idu0075.pold.ee/wsdl/shopApp", wsdlLocation = "WEB-INF/wsdl/shopWS/shop.wsdl")
public class shopWS {
    ArrayList<ShopIdentity> shopIdentities = new ArrayList<ShopIdentity>();
    
    private static int shopIdCounter = 1;
    
    private Request request = new Request();
    
    /**
     * main constructor. register products.
     */
    public shopWS(){
        super();
//        request.registerResource(new AmazonIdentity(++shopIdCounter));
        request.registerResource(new EbayIdentity(++shopIdCounter));
    }

    /**
     * Get Products
     * @param params
     * @return 
     */
    public ee.pold.idu0075.shop.FindProductsResponseType findProducts(ee.pold.idu0075.shop.FindProductsRequestType params) {
        
        FindProductsResponseType response = new FindProductsResponseType();
        FindProductsResponseType.Products responseProducts = new FindProductsResponseType.Products();
        response.setProducts(responseProducts);
        
        // Prepare filter
        ProductsFilter filter = new ProductsFilter();
        filter.setKeyword(params.getKeyword());
        if (params.getMinPrice() != null && params.getMinPrice().doubleValue() >= 0D) {
            filter.setMin_price(params.getMinPrice().doubleValue());
        }
        
        if (params.getMaxPrice() != null && params.getMaxPrice().doubleValue() >= 0D) {
            filter.setMin_price(params.getMaxPrice().doubleValue());
        }
        
        filter.setLimit(10);


        // Get products - this will send request and retrieve the list of products.
        ArrayList<Product> products = request.getProducts(filter);

        Iterator<Product> iterator = products.iterator();
        while(iterator.hasNext()){
            Product product = iterator.next();
            response.getProducts().getProduct().add(product);
        }
        
        return response;
    }

    /**
     * Get list of shop identities.
     * @param params
     * @return 
     */
    public ee.pold.idu0075.shop.GetShopsIdentitiesResponseType getShopsIdentities(ee.pold.idu0075.shop.GetShopsIdentitiesRequestType params) {
        GetShopsIdentitiesResponseType response = new GetShopsIdentitiesResponseType();
        GetShopsIdentitiesResponseType.Shops shops = new GetShopsIdentitiesResponseType.Shops();
        response.setShops(shops);
        
        Iterator<AbstractIdentity> iterator = request.getRegisteredShops().iterator();
        while(iterator.hasNext()){
            AbstractIdentity _shop = iterator.next();
            ShopIdentity shop = new ShopIdentity();
            shop.setId(BigInteger.valueOf(_shop.getId()));
            shop.setName(_shop.getName());
            response.getShops().getShopIdentity().add(shop);
        }
       
        return response;
    }

    public ee.pold.idu0075.shop.GetCategoriesListResponseType getCategoriesList(ee.pold.idu0075.shop.GetCategoriesListRequestType params) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public ee.pold.idu0075.shop.CalculatePriceWithDiscountResponseType calculatePriceWithDiscount(ee.pold.idu0075.shop.CalculatePriceWithDiscountRequestType params) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
