/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ee.pold.idu0075.shop;

import java.math.BigDecimal;
import java.math.BigInteger;
import ee.pold.idu0075.shop.resources.Request;
import ee.pold.idu0075.shop.resources.ebay.EbayIdentity;
import java.util.ArrayList;
import java.util.Iterator;
/**
 *
 * @author priitpl
 */
public class shopService 
{
    private static int idC = 0;
    private static int idP = 0;
    
    private Categories categories = new Categories();
    private FindProductsResponseType.Products products = new FindProductsResponseType.Products();
    
    public shopService(){
        Category electronics = new Category();
        electronics.setName("Electronics");
        electronics.setId(BigInteger.valueOf(++idC));
        
        Category tv = new Category();
        tv.setName("TV");
        tv.setId(BigInteger.valueOf(++idC));
        
        Category cameras = new Category();
        cameras.setName("Cameras");
        cameras.setId(BigInteger.valueOf(++idC));
        
        categories.getCategory().add(electronics);
        categories.getCategory().add(tv);
        categories.getCategory().add(cameras);
        
        Request request = new Request();
//		request.registerResource(new AmazonIdentity());
        request.registerResource(new EbayIdentity());

        ProductsFilter filter = new ProductsFilter();
        filter.setKeyword("Note 3");
        filter.setMin_price(100D);
        filter.setLimit(10);


        // Get products - this will send request and retrieve the list of products.
        ArrayList<Product> products = request.getProducts(filter);

        Iterator<Product> iterator = products.iterator();
        while(iterator.hasNext()){
                Product product = iterator.next();
                System.out.println(product);
        }
              
    }
    
    
    public Product create(String name, int categoryId, double price){
        Product p = create(name, price);
        p.setName(name);
        return p;
    }
    public Product create(String name){
        Product p = new Product();
        p.setName(name);
        return p;
    }
    public Product create(String name, double price){
        Product p = new Product();
        p.setName(name);
        p.setPrice(BigDecimal.valueOf(price));
        return p;
    }
    
    public FindProductsResponseType.Products getProducts(){
        return products;
    }
    public Categories getCategories(){
        return categories;
    }
}
