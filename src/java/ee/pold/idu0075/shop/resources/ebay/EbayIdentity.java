package ee.pold.idu0075.shop.resources.ebay;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import ee.pold.idu0075.shop.Product;
import ee.pold.idu0075.shop.ProductsFilter;
import ee.pold.idu0075.shop.RequestHelper;
import ee.pold.idu0075.shop.resources.AbstractIdentity;
import java.math.BigDecimal;
import java.math.BigInteger;

public class EbayIdentity extends AbstractIdentity{

	private final String DEVID = "001e0656-2ecc-43b0-87fc-d434e9e342b8";
	
	private final String AppID = "PriitPld-0107-4972-b418-cc658308f96a";

	private final String CertID = "ece1b39d-d740-41b7-80c2-ee6ee032b43d";
	
	
        public EbayIdentity(){
            super();
        }
        
        public EbayIdentity(int id){
            super(id);
        }
        
	@Override
	public String getName() {
		return "eBay"; 
	}
	
	@Override
	public ArrayList<Product> getProducts(ProductsFilter filter) {
		
            ArrayList<Product> products = new ArrayList<Product>();
		
//		http://svcs.ebay.com/services/search/FindingService/v1? 
//			 OPERATION-NAME=findItemsAdvanced& 
//			 SERVICE-VERSION=1.9.0& 
//			 SECURITY-APPNAME=YourAppID& 
//			 RESPONSE-DATA-FORMAT=XML& 
//			 REST-PAYLOAD& 
//			 keywords=nikon+d5000+digital+slr+camera& 
//			 itemFilter(0).name=Condition& 
//			 itemFilter(0).value=New& 
//			 itemFilter(1).name=MaxPrice& 
//			 itemFilter(1).value=750.00& 
//			 itemFilter(1).paramName=Currency& 
//			 itemFilter(1).paramValue=USD& 
//			 itemFilter(2).name=TopRatedSellerOnly& 
//			 itemFilter(2).value=true& 
//			 itemFilter(3).name=ReturnsAcceptedOnly& 
//			 itemFilter(3).value=true& 
//			 itemFilter(4).name=ExpeditedShippingType& 
//			 itemFilter(4).value=Expedited& 
//			 itemFilter(5).name=MaxHandlingTime& 
//			 itemFilter(5).value=1 

            String url = "http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsAdvanced" 
                            + "&SERVICE-VERSION=1.12.0"
                            + "&SECURITY-APPNAME=" + AppID
                            + "&RESPONSE-DATA-FORMAT=JSON"
                            + "&REST-PAYLOAD"
                            + "&paginationInput.entriesPerPage=" + filter.getLimit()
                            + "&paginationInput.pageNumber=" + 1
                            + "&keywords=" + filter.getKeyword();

            String data = RequestHelper.sendGetRequest(url);

            System.out.println(data);
            return parse(data);
	}
	
	/**
         * Parse data.
         * @param data 
         */
	private ArrayList<Product> parse(String data){
            ArrayList<Product> products = new ArrayList<Product>();
            try{
                JSONObject json = new JSONObject(RequestHelper.parseUnicodeJsonString(data));
                JSONArray _objects = json.getJSONArray("findItemsAdvancedResponse")
                                .getJSONObject(0)
                                .getJSONArray("searchResult").getJSONObject(0).getJSONArray("item");
                for(int i=0; i < _objects.length(); i++){
                        JSONObject _item = _objects.getJSONObject(i);
                        Product product = new Product();
//				product.setProductId(_item.getJSONArray("itemId").getString(0));
                        product.setName(_item.getJSONArray("title").getString(0));
//				product.setImageUrl(_item.getJSONArray("galleryURL").getString(0));
//				product.setUrl(_item.getJSONArray("viewItemURL").getString(0));
                        product.setPrice(new BigDecimal(_item.getJSONArray("sellingStatus").getJSONObject(0)
                                        .getJSONArray("currentPrice").getJSONObject(0)
                                        .getString("__value__")));
                        product.setImageUrl(_item.getJSONArray("galleryURL").getString(0));
                        product.setProductUrl(_item.getJSONArray("viewItemURL").getString(0));
                        product.setShopIdentity(BigInteger.valueOf(getId()));

                        products.add(product);
                        System.out.println(product);

                }
//			imagesTotal = Integer.valueOf(json.get("count").toString());
            } catch(Exception e){
                    System.out.println("ERROR: PanoramioResponse.parse: " + e.toString());
            }
            return products;
	}
	
	

}
