package ee.pold.idu0075.shop.resources;

import java.util.ArrayList;

import ee.pold.idu0075.shop.Product;
import ee.pold.idu0075.shop.ProductsFilter;

abstract public class AbstractIdentity{

    /**
     * Identity identificator.
     */
    private int id;
    
    /**
     * Get products list.
     * @param filter
     * @return 
     */
    abstract public ArrayList<Product> getProducts(ProductsFilter filter);
    
    /**
     * Get identity name
     * @return 
     */
    abstract public String getName();
    
    public AbstractIdentity(){
        
    }
    public AbstractIdentity(int id){
        this();
        setId(id);
    }
    /**
     * set ID
     * @param id 
     */
    public void setId(int id){}
    
    /**
     * Get ID
     * @return 
     */
    public int getId(){
        return id;
    }
}
