package ee.pold.idu0075.shop.resources;

import java.util.ArrayList;
import java.util.Iterator;

import ee.pold.idu0075.shop.Product;
import ee.pold.idu0075.shop.ProductsFilter;

public class Request {
	
    private ArrayList<AbstractIdentity> resources = new ArrayList<AbstractIdentity>();


    public Request(){

    }

    /**
     * Register identity - resource type like eBay, Amazon etc.
     * @param identity 
     */
    public void registerResource(AbstractIdentity identity) {
            resources.add(identity);
    }
    
    /**
     * Get list of registered shops
     * @return 
     */
    public ArrayList<AbstractIdentity> getRegisteredShops(){
        return resources;
    }

    /**
     * Get products. Use given filter.
     * @param filter
     * @return 
     */
    public ArrayList<Product> getProducts(ProductsFilter filter){
        System.out.println("INSEDE REQUEST.");
        ArrayList<Product> products = new ArrayList<Product>();

        Iterator<AbstractIdentity> iterator = resources.iterator();

        while(iterator.hasNext()){
            System.out.println("THIS IS INSIDE REQUEST");
            AbstractIdentity identity = iterator.next();
            ArrayList<Product> identityProducts = identity.getProducts(filter);
            if (identityProducts == null || identityProducts.size() == 0 ) {
                    continue;
            }

            products.addAll(identityProducts);

        }

        return products;
    }
}
