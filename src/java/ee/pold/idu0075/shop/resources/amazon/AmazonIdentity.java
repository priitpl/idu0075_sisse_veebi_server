package ee.pold.idu0075.shop.resources.amazon;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.amazon.advertising.api.sample.SignedRequestsHelper;

import ee.pold.idu0075.shop.Product;
import ee.pold.idu0075.shop.ProductsFilter;
import ee.pold.idu0075.shop.resources.AbstractIdentity;
import ee.pold.idu0075.shop.resources.amazon.xmlhandler.ItemSearch;

public class AmazonIdentity extends AbstractIdentity{

	public static final String USER_AGENT = "chrome";
	
	/*
     * Your AWS Access Key ID, as taken from the AWS Your Account page.
     */
    private static final String AWS_ACCESS_KEY_ID = "AKIAJ3YGLPWZHICMX4JA";

    /*
     * Your AWS Secret Key corresponding to the above ID, as taken from the AWS
     * Your Account page.
     */
    private static final String AWS_SECRET_KEY = "qT4IE6Ee4GtFPtGeSA+RiEP4rAhl/iTCctYvV+TZ";
    
    private static final String ENDPOINT = "ecs.amazonaws.co.uk";
    
    /*
     * The Item ID to lookup. The value below was selected for the US locale.
     * You can choose a different value if this value does not work in the
     * locale of your choice.
     */
    private static final String ITEM_ID = "0545010225";

    public AmazonIdentity(){
        super();
    }
    public AmazonIdentity(int id){
        super(id);
    }

	@Override
	public String getName() {
		return "Amazon";
	}
	
	public void test() {
        /*
         * Set up the signed requests helper 
         */
        SignedRequestsHelper helper;
        try {
            helper = SignedRequestsHelper.getInstance(ENDPOINT, AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        
        String requestUrl = null;
        String title = null;

        /* The helper can sign requests in two forms - map form and string form */
        
        /*
         * Here is an example in map form, where the request parameters are stored in a map.
         */
        System.out.println("Map form example:");
        Map<String, String> params = new HashMap<String, String>();
        params.put("Service", "AWSECommerceService");
        params.put("Version", "2011-08-01");
        params.put("Operation", "ItemSearch");
        params.put("Condition", "All");
        params.put("Availability", "Available");
        params.put("SearchIndex", "Toys");
//        params.put("ItemId", ITEM_ID);
        params.put("ResponseGroup", "Small");
        params.put("Keywords", "iPad mini");
        params.put("AssociateTag", "httpsisseveep-20");

        requestUrl = helper.sign(params);
        System.out.println("Signed Request is \"" + requestUrl + "\"");

        title = fetchTitle(requestUrl);
        System.out.println("Signed Title is \"" + title + "\"");
        System.out.println();

        /* Here is an example with string form, where the requests parameters have already been concatenated
         * into a query string. */
        System.out.println("String form example:");
        String queryString = "Service=AWSECommerceService&Version=2009-03-31&Operation=ItemLookup&ResponseGroup=Small&ItemId="
                + ITEM_ID;
        requestUrl = helper.sign(queryString);
        System.out.println("Request is \"" + requestUrl + "\"");

        title = fetchTitle(requestUrl);
        System.out.println("Title is \"" + title + "\"");
        System.out.println();

    }

    /*
     * Utility function to fetch the response from the service and extract the
     * title from the XML.
     */
    private static String fetchTitle(String requestUrl) {
    	
    	System.out.println("Send request to:");
    	System.out.println(requestUrl);
    	
    	ItemSearch itemSearchXmlHandler = new ItemSearch();
    	
    	String data = sendGetRequest(requestUrl);
    	try { 
			// Create InputStrem from text for parsing
			InputStream stream = new ByteArrayInputStream(data.getBytes("UTF-8"));
			XMLReader xr = XMLReaderFactory.createXMLReader();
			xr.setContentHandler(itemSearchXmlHandler);
			xr.parse( new InputSource(stream));
    	} catch(Exception e){
    		System.out.println("ERROR: " + e.getMessage());
    	}
		
		
        String title = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(requestUrl);
            System.out.println(doc);
            Node titleNode = doc.getElementsByTagName("Title").item(0);
            title = titleNode.getTextContent();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return title;
    }
    
		
	@Override
	public ArrayList<Product> getProducts(ProductsFilter filter) {
		
		ArrayList<Product> products = new ArrayList<Product>();
 
		test();
//		products.add(new Product(this).setName("Test Name").setPrice("123.23").setDescription("Some unknown stuff... nevert used and wount be"));
		
		return products;
	}
	
	protected void dummy(){
		try {
//		http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords
//		   &SERVICE-VERSION=1.0.0
//		   &SECURITY-APPNAME=YourAppID
//		   &RESPONSE-DATA-FORMAT=XML
//		   &REST-PAYLOAD
//		   &keywords=harry%20potter%20phoenix
				   
//			String url = "https://selfsolve.apple.com/wcResults.do";
			String url = "http://svcs.ebay.com/services/search/FindingService/v1";
			try{
				
				URL obj = new URL(url);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//				HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		 
				//add reuqest header
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", USER_AGENT);
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		 
//				String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
				String urlParameters = "OPERATION-NAME=findItemsByKeywords" 
					+ "&SERVICE-VERSION=1.0.0" + "&SECURITY-APPNAME=YourAppID"
					+ "&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD"
					+ "&keywords=harry%20potter%20phoenix";
		 
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();
		 
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Post parameters : " + urlParameters);
				System.out.println("Response Code : " + responseCode);
		 
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
		 
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
			} catch(Exception e){
				System.out.println(e.getMessage());
			}
		} catch (Exception e){
			
		}
	}
	
	/**
	 * Send request to given URL
	 * @param urlString
	 * @return
	 */
	public static String sendGetRequest(String urlString){
		String responseData = "";
		try {
			URL url = new URL(urlString);
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			url = uri.toURL();
			String urlStr = url.toString();
			
			System.out.println("Send request to: " + urlStr);
			
		    URLConnection yc = url.openConnection();
		    yc.setConnectTimeout(2000);
		    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		    String inputLine;
		
		    while ((inputLine = in.readLine()) != null) {
		    	responseData += inputLine;
		    }
		    in.close();
		} catch(Exception e){
			System.out.println(e.toString());
		}
		
        return responseData;
	}
	
	/**
	 * Send request to given URL
	 * @param urlString
	 * @return
	 */
	public static String sendPostRequest(String urlString){
		String responseData = "";
		try{
			URL obj = new URL(urlString);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	//		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	 
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	 
	//		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
			String urlParameters = "OPERATION-NAME=findItemsByKeywords" 
				+ "&SERVICE-VERSION=1.0.0" + "&SECURITY-APPNAME=YourAppID"
				+ "&RESPONSE-DATA-FORMAT=XML&REST-PAYLOAD"
				+ "&keywords=harry%20potter%20phoenix";
	 
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
	//		System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
		
		} catch(Exception e){
			
		}
        return responseData;
	}

}
